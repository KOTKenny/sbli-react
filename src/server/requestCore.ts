const endpoint: string = 'http://localhost/APP_MVC/WizardFormXml?AskSSN=true&formDir=direct';

export async function getPageXMLString(appId : string, formId: string): Promise<string>{
    const requestMetadata = {
        headers: {
          'Content-Type': 'text/xml; charset=utf-8',
        }
    }
    return fetch(endpoint.concat('&AppID=').concat(appId).concat('&formId=').concat(formId))
            .then(response => response.text())
  }
  