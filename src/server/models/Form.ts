export interface IForm {
    name: string,
    attributes: IFormAttributes,
    children: Array<IControl>,
    value: string
}

export interface IFormAttributes {
    name: string
    nextFormId: string,
    prevFormId: string
}

export interface IControl {
    name: string,
    attributes: ICtrlAttributes,
    children: Array<IControl>,
    value: string
}

export interface ICtrlAttributes {
    type: string,
    readonly: string,
    Answer: string,
    ControlID: string,
    TextValue: string,
    hidden: string,
    q: string,
    mode?: string
    visibility?: string
    tc : string
}

export interface IVisibilityGroup {
    group: IVisibilityRule
}

export interface IVisibilityRule {
    data?: string,
    field?: string,
    operator: string,
    rules: Array<IVisibilityRule>
}