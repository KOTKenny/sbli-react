var XMLParser = require('react-xml-parser');

export function xml2json(xmlText: string){
    var obj = new XMLParser().parseFromString(xmlText);

    return obj;
}

export function getFormFromXml(xmlText: string): Array<object>{
    var obj = new XMLParser().parseFromString(xmlText).getElementsByTagName('Form');

    return obj;
}