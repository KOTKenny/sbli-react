import { Subtitle } from "../../components/Subtitle";
import { String } from "../../components/String";
import { IControl, IVisibilityGroup, IVisibilityRule } from "../models/Form"
import { Midname } from "../../components/Midname";
import { Combobox } from "../../components/Combobox";
import { Checkbox } from "../../components/Checkbox";
import { ComplexControl } from "../../components/ComplexControl";
import { Switchboxes } from "../../components/Switchboxes";
import { Boolean } from "../../components/Boolean";

interface ControlProps {
    index: number
    ctrl: IControl
    formControls: Array<IControl>
}

export const RenderControl: React.FC<ControlProps> = (props: ControlProps) => {
    let ctrlAttrs = props.ctrl.attributes;
    let ctrlType = ctrlAttrs.type;
    let q = ctrlAttrs.q;
    // console.log(ctrlAttrs.ControlID);

    //Visibility logic
    if (ctrlAttrs.hidden == "true" ||
        (ctrlAttrs.visibility != undefined &&
            !IsVisible(JSON.parse(ctrlAttrs.visibility.replace(/&quot;/g, '"')), props.formControls))) {

        return (
            <div key={props.index} style={{ display: 'none' }}></div>
        )
    }

    //Can implement factory
    switch (ctrlType) {
        case "Subtitle":
            return <Subtitle key={props.index} ctrl={props.ctrl} />
        case "String":
        case "Money":
        case "Email":
            return <String key={props.index} ctrl={props.ctrl} />
        case "Midname":
            return <Midname key={props.index} ctrl={props.ctrl} />
        case "Combobox":
        case "State":
            return <Combobox key={props.index} ctrl={props.ctrl} />
        case "Checkbox":
            return <Checkbox key={props.index} ctrl={props.ctrl} />
        case "Ssn":
        case "Zip":
        case "Phone":
            return <ComplexControl key={props.index} ctrl={props.ctrl} separator={/(?:-)|(?:ex)/} />
        case "Dob2":
            return <ComplexControl key={props.index} ctrl={props.ctrl} separator="/" />
        case "Switchbox":
            return <Switchboxes key={props.index} ctrl={props.ctrl} />
        case "Boolean":
            return <Boolean key={props.index} ctrl={props.ctrl} />
        default:
            return (
                <div key={props.index} className="col-6">{q}</div>
            )
    }
}

function IsVisible(visibilityObj: IVisibilityGroup, controls: Array<IControl>): boolean {
    let isVisible = true;
    // console.log(visibilityObj);
    if (visibilityObj == undefined)
        return isVisible;

    visibilityObj.group.rules.forEach(rule => {
        let ctrl = controls.find(c => c.attributes.ControlID == rule.field)

        if (!ctrl) {
            isVisible = false;
        }
        else {
            if (rule.data && ctrl.attributes.Answer) {
                if (!VisibilityConditionChecker(ctrl.attributes.Answer, rule.operator, rule.data))
                    isVisible = false;
            }
            else
                isVisible = false;
        }
    });

    return isVisible;
}

function VisibilityConditionChecker(ans: string, operator: string, data: string) {
    switch (operator) {
        case "=":
            return ans == data;
        case "~":
            // let re = new RegExp(data);
            // return re.test(ans);
            return true;
        default:
            return false;
    }
}