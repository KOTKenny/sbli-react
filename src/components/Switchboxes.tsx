import { useEffect, useState } from "react"
import { IControl } from "../server/models/Form"
import { ControlLabel } from "./Common/ControlLabel"
import { CommonControlInputLayout } from "./Layouts/CommonControlInputLayout"
import { CommonControlLayout } from "./Layouts/CommonControlLayout"
import { CommonLabelLayout } from "./Layouts/CommonLabelLayout"

interface ISwitchboxes {
    ctrl: IControl
}

export const Switchboxes: React.FC<ISwitchboxes> = (props: ISwitchboxes) => {
    const [value, setValue] = useState(props.ctrl.attributes.Answer)

    return (
        <CommonControlLayout>
            <CommonLabelLayout>
                <ControlLabel question={props.ctrl.attributes.q}></ControlLabel>
            </CommonLabelLayout>
            <div className="ctrl-layout col-12">
                <div className="d-flex flex-wrap gap-3">
                    {
                        props.ctrl.children[0].children.map((sb, index) => {
                            let sbId = props.ctrl.attributes.ControlID.concat('_').concat(index.toString());

                            return (
                                <div className="form-check p-0">
                                    <input className="form-check-input" type="radio" name={props.ctrl.attributes.ControlID}
                                        id={sbId} onChange={() => setValue(sb.attributes.tc)} checked={sb.attributes.tc == value} />
                                    <label className="form-check-label" htmlFor={sbId}>
                                        {sb.value}
                                    </label>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </CommonControlLayout>
    )
}