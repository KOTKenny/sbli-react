import { useState } from "react"
import { IControl } from "../server/models/Form"
import { CommonControlLayout } from "./Layouts/CommonControlLayout";
import { CommonLabelLayout } from "./Layouts/CommonLabelLayout";
import { ControlLabel } from "./Common/ControlLabel";
import { CommonControlInputLayout } from "./Layouts/CommonControlInputLayout";

interface IComplexControl {
    ctrl: IControl
    separator: string | RegExp
}

export const ComplexControl: React.FC<IComplexControl> = (props: IComplexControl) => {
    const numberOfInputs = props.ctrl.attributes.mode
        ? Number.parseInt(props.ctrl.attributes.mode)
        : 1;

    const ans = parseAnswer();

    function parseAnswer(): Array<string> {
        let ans = props.ctrl.attributes.Answer ? props.ctrl.attributes.Answer.split(props.separator) : Array(numberOfInputs).fill("");

        return ans;
    }

    return (
        <CommonControlLayout>
            <CommonLabelLayout>
                <ControlLabel question={props.ctrl.attributes.q}></ControlLabel>
            </CommonLabelLayout>
            <CommonControlInputLayout>
                <div className="d-flex gap-2">
                    {Array(numberOfInputs).fill(0).map((val, index) => {

                        return (
                            <ComplexControlInput index={index} ans={ans} ctrl={props.ctrl} />
                        )
                    })}
                </div>
            </CommonControlInputLayout>
        </CommonControlLayout>
    )
}

interface IComplexControlInput {
    index: number
    ans: Array<string>
    ctrl: IControl
}

const ComplexControlInput: React.FC<IComplexControlInput> = (props: IComplexControlInput) => {
    const [value, setValue] = useState(props.ans[props.index]);

    return (
        <input id={props.ctrl.attributes.ControlID.concat('_').concat(props.index.toString())} type="text"
            placeholder={Array(props.ans[props.index].length + 1).join('X')} className="form-control" value={value}
            disabled={props.ctrl.attributes.readonly == "true"} onChange={e => setValue(e.target.value)} />
    )
}