import { useState } from "react"
import { IControl } from "../server/models/Form"
import { ControlLabel } from "./Common/ControlLabel"
import { CommonControlInputLayout } from "./Layouts/CommonControlInputLayout"
import { CommonControlLayout } from "./Layouts/CommonControlLayout"
import { CommonLabelLayout } from "./Layouts/CommonLabelLayout"

interface IMidname {
    ctrl: IControl
}

export const Midname: React.FC<IMidname> = (props: IMidname) => {
    const [value, setValue] = useState(props.ctrl.attributes.Answer);

    return (
        <CommonControlLayout>
            <CommonLabelLayout>
                <ControlLabel question={props.ctrl.attributes.q}></ControlLabel>
            </CommonLabelLayout>
            <CommonControlInputLayout>
                <input type="text" className="form-control" value={value}
                    disabled={props.ctrl.attributes.readonly == "true"} onChange={e => setValue(e.target.value)} />
            </CommonControlInputLayout>
        </CommonControlLayout>
    )
}