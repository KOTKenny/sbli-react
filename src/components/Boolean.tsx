import { useEffect, useState } from "react"
import { IControl } from "../server/models/Form"
import { ControlLabel } from "./Common/ControlLabel"
import { CommonControlInputLayout } from "./Layouts/CommonControlInputLayout"
import { CommonControlLayout } from "./Layouts/CommonControlLayout"
import { CommonLabelLayout } from "./Layouts/CommonLabelLayout"

interface IBoolean {
    ctrl: IControl
}

export const Boolean: React.FC<IBoolean> = (props: IBoolean) => {
    const [value, setValue] = useState(props.ctrl.attributes.Answer)
    const ans = Array<string>("Yes", "No");

    return (
        <CommonControlLayout>
            <CommonLabelLayout>
                <ControlLabel question={props.ctrl.attributes.q}></ControlLabel>
            </CommonLabelLayout>
            <div className="ctrl-layout col-12">
                <div className="d-flex gap-3">
                    {
                        ans.map((sb, index) => {
                            let sbId = props.ctrl.attributes.ControlID.concat('_').concat(index.toString());

                            return (
                                <div className="form-check p-0">
                                    <input className="form-check-input" type="radio" name={props.ctrl.attributes.ControlID}
                                        id={sbId} onChange={() => setValue(sb)} checked={sb == value} />
                                    <label className="form-check-label" htmlFor={sbId}>
                                        {sb}
                                    </label>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        </CommonControlLayout>
    )
}