import { IControl } from "../server/models/Form"

interface ISubtitle{
    ctrl: IControl
}

export const Subtitle: React.FC<ISubtitle> = (props: ISubtitle) => {
    return (
        <label className="subTitle">
            {props.ctrl.attributes.q}
        </label>
    )
}

