import { useEffect, useState } from "react"
import { IControl } from "../server/models/Form"
import { CommonControlLayout } from "./Layouts/CommonControlLayout"
import { CommonLabelLayout } from "./Layouts/CommonLabelLayout"
import { ControlLabel } from "./Common/ControlLabel"
import { CommonControlInputLayout } from "./Layouts/CommonControlInputLayout"

interface IString {
    ctrl: IControl
}

export const Combobox: React.FC<IString> = (props: IString) => {
    const [selectedElem, setSelectedElem] = useState(props.ctrl.attributes.TextValue);

    return (
        <CommonControlLayout>
            <CommonLabelLayout>
                <ControlLabel question={props.ctrl.attributes.q}></ControlLabel>
            </CommonLabelLayout>
            <CommonControlInputLayout>
                <div className="dropdown">
                    <button id={props.ctrl.attributes.ControlID} className="btn dropdown-toggle w-100 d-flex 
                        justify-content-between align-items-center" value={props.ctrl.attributes.TextValue} 
                        type="button" data-bs-toggle="dropdown" 
                        aria-expanded="false" disabled={props.ctrl.attributes.readonly == "true"}>
                            {selectedElem}
                    </button>
                    <DropDownList setSelectedElem={setSelectedElem} valuesArray={props.ctrl.children[0].children} />
                </div>
            </CommonControlInputLayout>
        </CommonControlLayout>
    )
}

interface IDropDownProps {
    valuesArray: Array<IControl>
    setSelectedElem: any
}

const DropDownList: React.FC<IDropDownProps> = (props: IDropDownProps) => {
    return (
        <ul className="dropdown-menu">
            {props.valuesArray.map((value, index) => {
                return (
                    <li key={index}>
                        <a key={index} className="dropdown-item"
                            onClick={() => props.setSelectedElem(value.value)}>
                                {value.value}
                        </a>
                    </li>)
            })}
        </ul>

    )
}