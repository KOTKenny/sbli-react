interface ICommonControlInputLayoutProps{
    children?: Array<JSX.Element> | JSX.Element;
}

export const CommonControlInputLayout: React.FC<ICommonControlInputLayoutProps> = (props: ICommonControlInputLayoutProps) => {
    return (
        <div className="col-12 col-sm-6 col-xl-12">
           {props.children}
        </div>
    )
}