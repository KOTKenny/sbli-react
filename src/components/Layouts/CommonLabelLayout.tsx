interface ICommonLabelLayoutProps{
    children?: Array<JSX.Element> | JSX.Element
}

export const CommonLabelLayout: React.FC<ICommonLabelLayoutProps> = (props: ICommonLabelLayoutProps) => {
    return (
        <div className="ctrl-label-layout col-12 col-sm-6 col-xl-12">
           {props.children}
        </div>
    )
}