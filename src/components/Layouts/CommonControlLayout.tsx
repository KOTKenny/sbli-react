interface ICommonControlLayoutProps {
    children?: Array<JSX.Element> | JSX.Element;
}

export const CommonControlLayout: React.FC<ICommonControlLayoutProps> = (props: ICommonControlLayoutProps) => {
    return (
        <div className="ctrl-layout col-12 col-xl-6">
            <div className="row">
                {props.children}
            </div>
        </div>
    )
}