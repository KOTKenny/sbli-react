import { useState } from "react"
import { IControl } from "../server/models/Form"
import { ControlLabel } from "./Common/ControlLabel"
import { CommonControlInputLayout } from "./Layouts/CommonControlInputLayout"
import { CommonControlLayout } from "./Layouts/CommonControlLayout"
import { CommonLabelLayout } from "./Layouts/CommonLabelLayout"

interface ICheckbox {
    ctrl: IControl
}

export const Checkbox: React.FC<ICheckbox> = (props: ICheckbox) => {
    const [checked, setChecked] = useState(props.ctrl.attributes.Answer == "Yes")

    return (
        <div className="ctrl-layout col-12">
            <div className="row">
                <div className="form-check">
                    <input className="form-check-input" type="checkbox" value="" id={props.ctrl.attributes.ControlID}
                        onClick={() => setChecked(!checked)} checked={checked} 
                        disabled={props.ctrl.attributes.readonly == "true"}/>
                    <label className="form-check-label" htmlFor="flexCheckDefault">
                        {props.ctrl.attributes.q}
                    </label>
                </div>
            </div>
        </div>
    )
}