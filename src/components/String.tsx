import { useState } from "react"
import { IControl } from "../server/models/Form"
import { ControlLabel } from "./Common/ControlLabel"
import { CommonControlInputLayout } from "./Layouts/CommonControlInputLayout"
import { CommonControlLayout } from "./Layouts/CommonControlLayout"
import { CommonLabelLayout } from "./Layouts/CommonLabelLayout"

interface IString {
    ctrl: IControl
}

export const String: React.FC<IString> = (props: IString) => {
    const [value, setValue] = useState(props.ctrl.attributes.Answer);
    
    return (
        <CommonControlLayout>
            <CommonLabelLayout>
                <ControlLabel question={props.ctrl.attributes.q}></ControlLabel>
            </CommonLabelLayout>
            <CommonControlInputLayout>
                <input id={props.ctrl.attributes.ControlID} type="text" className="form-control" 
                    value={value} disabled={props.ctrl.attributes.readonly == "true"} onChange={e => setValue(e.target.value)} />
            </CommonControlInputLayout>
        </CommonControlLayout>
    )
}