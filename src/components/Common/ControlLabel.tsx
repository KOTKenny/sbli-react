interface IControlLabelProps{
    children?: Array<JSX.Element> | JSX.Element
    question: string
}

export const ControlLabel: React.FC<IControlLabelProps> = (props: IControlLabelProps) => {
    return (
        <label className="question-text">
           {props.question}
        </label>
    )
}