import { FC } from "react";
import { IForm } from "./server/models/Form";
import { RenderControl } from "./server/helpers/renderHelper";

interface FormProps {
    form: IForm
}

export const RenderBody: React.FC<FormProps> = (props: FormProps) => {
    return (
        <div className="row">
            {props.form.children.map((val, index) => {
                return RenderControl({ index: index, ctrl: val, formControls: props.form.children })
            })}
        </div>
    )
}