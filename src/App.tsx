import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import { getPageXMLString } from './server/requestCore';
import { getFormFromXml, xml2json } from './server/helpers/xmlParser';
import { RenderBody } from './RenderBody';
import { IForm } from './server/models/Form';
import { ButtonsPanel } from './pages/components/common/ButtonsPanel';

function App() {
  const [form, setForm] = useState<null | IForm>(null);
  const [formId, setFormId] = useState('la_partB_2023_ak_product_information'); //start page init

  //Temporary jumpToForm logic
  function loadForm(formId: string){
    setFormId(formId);
    setForm(null);
  }

  //Temporary Form load logic
  if (form == null)
    getPageXMLString('31891d5d-c97e-4e1c-b57b-8834df30b509', formId).then(result => {
      console.log(getFormFromXml(result)[0])
      setForm(getFormFromXml(result)[0] as IForm);
    })

  if (form == null)
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  else
    return (
      <div>
        <div id="TopPane">
          <div className='modern-header-container'>
            <div className="modern-header-logo">
            </div>
            <div className="modern-header-text">
              <h1>Your online application: Part 2</h1>
            </div>
          </div>
        </div>
        <div id="MainPane">
          <div className="main-pane-header">
            <h2>{form.attributes.name}</h2>
          </div>
          <div className="main-pane-questions">
            <RenderBody form={form} />
            <ButtonsPanel loadForm={loadForm} nextFormId={form.attributes.nextFormId} prevFormId={form.attributes.prevFormId} />
          </div>
        </div>
      </div>
    );
}

export default App;
