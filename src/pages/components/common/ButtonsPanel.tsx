interface IButtonsPanelProps {
    loadForm: any,
    nextFormId: string,
    prevFormId: string
}

export const ButtonsPanel: React.FC<IButtonsPanelProps> = (props: IButtonsPanelProps) => {
    return (
        <div className="buttons-panel mt-5">
            <div className="row">
                <div className="col-6 mb-3 bottom-btn-container">
                    <input id="buttonPrev" type="button" name="buttonPrev" className="btn btn-outline-primary bottom-menu-btn 
                            link-bottom-button w-100" value="< Back" onClick={() => props.loadForm(props.prevFormId)} disabled={!props.prevFormId} />
                </div>
                <div className="col-6 mb-3 bottom-btn-container">
                    <input id="buttonNext" type="button" name="buttonNext" className="btn btn-outline-primary bottom-menu-btn
                        w-100" value="Next >" onClick={() => props.loadForm(props.nextFormId)} disabled={!props.nextFormId} />
                </div>
                <div className="col-12 bottom-btn-container finish-btn">
                    <input id="buttonFinish" type="button" name="buttonFinish" className="btn btn-primary bottom-menu-btn
                        w-100" value="Save" />
                </div>
            </div>
        </div>
    )
}